

'''
hogyun choi / september 2, 2021
Q9.3: search rotated list

arr = [ 20, 21, 22, 23, 24, 1, 2, 3 , 4, 5 ]
arr = [ 1, 2, 3 , 4, 5, 6, 7, 20, 21, 22, 23 ,24 ]

# assumption is one half is always sorted sequence
# only two possibilities for rotated sorted list,
# either all sequential, or one half is sequential

[ 1 2 3 4 5 ]
[ 5 1 2 3 4 ]


1. check which half is sequential via a[l] < a[m]
2. on sequential side, check if value is in the bounds of a[l] < a[m]
   if not, check other side, if it is, keep searching this side by setting new left or right middle
'''


def search_rotated_list( value, sorted_rotated_list ):


	left = 0
	right = len(sorted_rotated_list) - 1
	middle = (left + right)/ 2

	a = sorted_rotated_list
	#print("l={}, r={}, m={}, v={}").format(left, right, middle, value)
	while( a[middle] != value ):
		#print("l={}, r={}, m={}, v={}").format(left, right, middle, value)
		if left > right:
			return -1
		

		if( a[left] < a[middle] ):
			# left side is sorted sequence
			if value > a[middle]:
				# not in this sequence
				left = middle + 1
			elif value >= a[left]:
				right = middle - 1
			else:
				left = middle + 1
		else:
			# right side is sorted sequence
			if value > a[right]:
				right = middle - 1
			elif value > a[middle]:
				left = middle + 1
			else:
				right = middle - 1

		middle = (left + right )/2

	return middle


a = [ 1, 2, 3, 4, 5 ]
print( search_rotated_list( 1, a ) )
print( search_rotated_list( 2, a ) )
print( search_rotated_list( 3, a ) )
print( search_rotated_list( 4, a ) )
print( search_rotated_list( 5, a ) )

a = [ 5, 6, 1, 2, 3, 4 ]
print( search_rotated_list( 5, a) )
print( search_rotated_list( 6, a) )
print( search_rotated_list( 1, a) )
print( search_rotated_list( 2, a) )
print( search_rotated_list( 3, a) )
print( search_rotated_list( 4, a) )
print( search_rotated_list( 99, a) )
