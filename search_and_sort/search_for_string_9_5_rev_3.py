

arr = ["aaa", "", "", "", "aab", "", "aabc", "aabcc", "", "", "", "bbb", "", "", "", "", "", "bbcc", "ccc", "", "", ""]
# array of strings is sorted, not including the empty strings


def search_for_string( string, array_of_strings ):

	left = 0
	right = len(array_of_strings) - 1
	middle = (left + right) / 2


	while( array_of_strings[middle] != string ):

		if left >= right:
			return middle
		# right is empty string, so iterate right until not empty
		while( array_of_strings[right] == "" and left <= right):
			right -= 1


		while( array_of_strings[middle] == "" and middle <= right):
			middle += 1

		if array_of_strings[middle] == string:
			return middle

		# string is left of middle, search the left side 
		if string < array_of_strings[middle]:
			right = middle - 1

		# search the right side
		else:
			left = middle + 1

		middle = (left + right) / 2


	return middle


print( search_for_string("aaa", arr) )