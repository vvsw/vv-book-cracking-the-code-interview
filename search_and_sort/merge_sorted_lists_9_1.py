'''
hogyun choi / september 2, 2021

give two sorted arrays A and B
A is large enough to hold B, merge A and B in sorted fashion

'''


def merge_sorted_lists(a, b):
	
	n = len(a) - 1  # index to last buffer spot
	i = 0
	# find the last element in the array
	while( a[i] != 0 ):
		i += 1
	i = i - 1	# point to the last element

	j = len(b) - 1

	while( i >= 0 and j >= 0):
		if a[i] > b[j]:
			a[n] = a[i]
			i -= 1
			n -= 1
		else:
			a[n] = b[j]
			j -= 1
			n -= 1

	# we should have left remainders in one list
	if i > 0:
		while (i >= 0):
			a[n] = a[i]
			i -= 1
			n -= 1
	else:
		while( j >= 0):
			a[n] = b[j]
			j -= 1
			n -= 1

	return a

a = [1,3,5,7,9,13,15,16,0,0,0,0,0,0,0,0,0]
b = [1,2,4,5,10,11,18,19,20]

print(merge_sorted_lists(a,b))



