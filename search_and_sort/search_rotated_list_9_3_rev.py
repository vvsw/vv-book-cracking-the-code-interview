

'''
hogyun choi / september 2, 2021
Q9.3: search rotated list
'''

a = [ 1, 2, 3, 4, 5 ]
a = [ 5, 6, 1, 2, 3, 4 ]

def search_rotated_list( value, sorted_rotated_list ):
	a = sorted_rotated_list

	left = 0
	right = len(a) -1
	middle = (left + right) / 2

	while( a[middle] != value ):

		if left > right:
			return -1

		if a[left] < a[middle]:
			# left side is sequential
			if value > a[middle]:
				# not on this side, check other side
				left = middle + 1
			elif value < a[left]:
				# not on this side
				left = middle + 1
			else:
				right = middle - 1

		else:
			# right side is sequential
			if value > a[right]:
				right = middle - 1
			elif value < a[middle]:
				right = middle - 1
			else:
				left = middle + 1

		middle = (left + right)/2
	
	return middle


a = [ 1, 2, 3, 4, 5 ]
print( search_rotated_list( 1, a ) )
print( search_rotated_list( 2, a ) )
print( search_rotated_list( 3, a ) )
print( search_rotated_list( 4, a ) )
print( search_rotated_list( 5, a ) )

a = [ 5, 6, 1, 2, 3, 4 ]
print( search_rotated_list( 5, a) )
print( search_rotated_list( 6, a) )
print( search_rotated_list( 1, a) )
print( search_rotated_list( 2, a) )
print( search_rotated_list( 3, a) )
print( search_rotated_list( 4, a) )
print( search_rotated_list( 99, a) )
