




arr = ["aaa", "", "", "", "aab", "", "aabc", "aabcc", "", "", "", "bbb", "", "", "", "", "", "bbcc", "ccc", "", "", ""]

def search_for_string( string, list_of_strings ):
	
	left = 0
	right = len(list_of_strings) - 1
	middle = (left + right)/2


	while( list_of_strings[middle] != string ):
		# middle is not string, keep searching
		# check if string is on left side or right side

		# nore more spaces to check, must be middle
		if left >= right:
			return middle

		# make sure right is not ""
		while(list_of_strings[right] == "" and left <= right ):
			right -= 1
		
		while(list_of_strings[middle] == "" and middle <= right):
			middle += 1

		if list_of_strings[middle] == string:
			return middle

		if string <= list_of_strings[middle]:
			# string is on the left side, search the left
			right = middle - 1
			middle = (left + right)/2

		else:
			# string is on right side, search the right
			left = middle + 1
			middle = (left + right)/2

	# we found the string, return index
	return middle



print(search_for_string("aabcc", arr))
print(search_for_string("bbb", arr))
print(search_for_string("aabc", arr))
print(search_for_string("aaa", arr))
print(search_for_string("ccc", arr))
