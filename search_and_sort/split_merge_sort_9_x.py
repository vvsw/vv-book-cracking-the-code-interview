
'''
hogyun choi / september 2, 2021

mergesort 


'''


def merge( sorted_left_list, sorted_right_list ):
	new_merged_list = []
	
	i = 0
	j = 0

	while(i < len(sorted_left_list) and j < len(sorted_right_list)):

		if( sorted_left_list[i] <= sorted_right_list[j] ):
			new_merged_list.append(sorted_left_list[i])
			i += 1
		else:
			new_merged_list.append(sorted_right_list[j])
			j += 1

	# we have exhausted one of the lists, add remainders to new list
	if i == len(sorted_left_list):
		while(j < len(sorted_right_list)):
			new_merged_list.append(sorted_right_list[j])
			j += 1
	else:
		while(i < len(sorted_left_list)):
			new_merged_list.append(sorted_left_list[i])
			i += 1

	return new_merged_list


def split_merge_sort( list ):
	
	# if list is size 1, not splittable
	if len(list) == 1:
		return list

	middle = len(list)/2

	sorted_left_list = split_merge_sort( list[:middle] )
	sorted_right_list = split_merge_sort( list[middle:] )

	return merge(sorted_left_list, sorted_right_list)




unsorted_list_a = [1,5,6,2,5,7,3,6,1,2,6,1,0]
print( split_merge_sort( unsorted_list_a ))

unsorted_list_b = [9,95,4,1,2,5,1,6,76,1,2,3,1,6,78,2,3,78,5,13,3,4,5,1,6,1,3,1,5,7,2,8,2,9,4,5]
print( split_merge_sort( unsorted_list_b ))