'''
hogyun choi / september 1, 2021
Q9.5

give an sorted array of strings, with empty spaces, return the index of a string

'''

arr = ["aaa", "", "", "", "aab", "", "aabc", "aabcc", "", "", "", "bbb", "", "", "", "", "", "bbcc", "ccc", "", "", ""]


def search_for_string( string, array_of_strings ):

	left = 0
	right = len(array_of_strings) - 1
	while( left < right ):
		print("left: {}").format(left)
		print("right: {}\n").format(right)


		while( left <= right and array_of_strings[right] == ""):
			right -= 1
		middle = (left + right) / 2

		if array_of_strings[middle] == "":
			while(array_of_strings[middle] == ""):
				middle += 1
		
		if array_of_strings[middle] == string:
			return middle


		if string < array_of_strings[middle]:

			right = middle - 1
		else:
			left = middle + 1


print(search_for_string( "aabc", arr))
print(search_for_string( "aaa", arr))


