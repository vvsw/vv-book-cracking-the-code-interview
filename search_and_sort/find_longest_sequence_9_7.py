
'''
hogyun choi / september 2, 2021
Q:9.7 find longest sequence given conditions (h>h_prev and w>w_prev)

* the question itself is not clear. but no one can be left out from the original sequence
'''



def find_longest_sequence( list_of_heights_weights ):
	sorted_list_of_heights_and_weights = sorted( list_of_heights_weights, key = lambda x: (x[0],x[1]))

	prev_height = 0
	prev_weight = 0
	current_found_sequence = []
	longest_sequence_found = []

	for (h,w) in list_of_heights_weights:

		if h >= prev_height and w >= prev_weight:
			prev_weight = w
			prev_height = h
			current_found_sequence.append((h,w))

			if len(current_found_sequence) > len(longest_sequence_found):
				longest_sequence_found = current_found_sequence

		else:
			prev_weight = 0
			prev_height = 0

			current_found_sequence = []
			current_found_sequence.append((h,w))
			# start searching again from this tuple	
	return longest_sequence_found

list_of_heights_weights = [(1,3), (4,2), (3,1), (5,1), (1,1), (7,4), (6,2), (4,2), (7,3)]
print( find_longest_sequence( list_of_heights_weights ) )

list_of_heights_weights = [(1,4),(2,3),(3,4),(5,6)]
print( find_longest_sequence( list_of_heights_weights ) )




