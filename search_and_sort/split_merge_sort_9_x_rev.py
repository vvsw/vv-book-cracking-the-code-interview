

def split_merge_sort( list ):
	
	if len(list) == 1:
		return list

	middle = len(list)/2

	left_sorted_list = split_merge_sort( list[:middle] )
	right_sorted_list = split_merge_sort( list[middle:] )

	return merge(left_sorted_list, right_sorted_list)


def merge( left_sorted_list, right_sorted_list ):
	
	new_merged_list = [0] * (len(left_sorted_list) + len(right_sorted_list))

	n = 0  # new list index
	i = 0  # left list index
	j = 0  # right list index

	while( i < len(left_sorted_list) and j < len(right_sorted_list) ):

		if left_sorted_list[i] < right_sorted_list[j]:
			new_merged_list[n] = left_sorted_list[i]
			n += 1
			i += 1
		else:
			new_merged_list[n] = right_sorted_list[j]
			n += 1
			j += 1

	# we have exhausted one of the lists, add reaminder to new list

	if i == len(left_sorted_list):
		while( j < len(right_sorted_list)):
			new_merged_list[n] = right_sorted_list[j]
			n += 1
			j += 1
	else:
		while( i < len(left_sorted_list)):
			new_merged_list[n] = left_sorted_list[i]
			n += 1
			i += 1

	assert(n == len(new_merged_list))
	return new_merged_list



print(split_merge_sort([6,1,2,6,1,7,8,1,3,5,1,7,44,7,2,32,6,2,6,1,7,2,47]))



