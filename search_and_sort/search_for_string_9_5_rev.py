'''

hogyun choi/ september 2, 2021

Q9.5: given a sorted array of strings, return the index of a particular string

simple binary search

'''



def search_array_of_strings( string, array_of_strings ):
	
	left = 0
	right = len(array_of_strings)
	middle = (left + right)/2

	while( array_of_strings[middle] != string):
		
		# no space between left and right, must be a hit
		if left >= right:
			return middle


		# need to search the left side
		if string < array_of_strings[middle]:
			right = middle - 1		# minus one since we checked middle already 
			middle = ( left + right )/2

		# otherwise search right side
		else:
			left = middle + 1
			middle = ( left + right)/2

	return middle

arr = ["aaa", "aaaa", "aaab", "aabb", "aabbc", "aabbcd", "aabbcdd", "aabbccddd", "aabbccdde", "baabbccddee", "baabbccddeeff" ]
print( search_array_of_strings("aabbc", arr ) )
print( search_array_of_strings("aaa", arr ) )
print( search_array_of_strings("baabbccddeeff", arr ) )

arr = [1,2,3,4,5,6,7,8,9,10]
print( search_array_of_strings(1, arr ) )
print( search_array_of_strings(2, arr ) )
print( search_array_of_strings(3, arr ) )
print( search_array_of_strings(4, arr ) )
print( search_array_of_strings(5, arr ) )
print( search_array_of_strings(6, arr ) )
print( search_array_of_strings(7, arr ) )
print( search_array_of_strings(8, arr ) )
print( search_array_of_strings(9, arr ) )
print( search_array_of_strings(10, arr ) )

