# hogyun choi / august 31 2021
# think about it this way, 
# prefix is empty, string is full

# first iteration, we want to create len(string) copies of prefix
# with every element of string at the first index of prefix 
# given : [a,b,c,d]
# ie: 	[a] , [b,c,d]
#     	[b] , [a,c,d]
#		[c] , [a,b,d]
#		[d]	, [a,b,c]

def permutations( prefix , string ):
	
	if(len(string) == 0):
		print(prefix)

	for char in string:
		prefix_with_char = prefix + char
		string_without_char = [x for x in string if x != char]
		
		permutations( prefix_with_char, string_without_char )

permutations("", "ABCD")