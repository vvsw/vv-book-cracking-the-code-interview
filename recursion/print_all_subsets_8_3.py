

def print_all_subsets(subset, string):
	
	if len(string) == 0: return
	for char in string:
		subset_with_char = subset + char
		print(subset_with_char)
		string_without_char = [x for x in string if x != char]
		print_all_subsets(subset_with_char, string_without_char)
		string = string_without_char

print_all_subsets("", "ABCDE")