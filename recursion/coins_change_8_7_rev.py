# hogyun choi august 31 2021


# how many ways in coin change, can you make n

# find all ways with each coin denom
# keep branching out from each with next denom

def count_coin_change_ways( n , denom ):
	
	next_denom = 0
	if denom == 25:
		next_denom = 10
	elif denom == 10:
		next_denom = 5
	elif denom == 5:
		next_denom = 1
	else:
		return 1   # we've made n! 

	num_of_denom = 0
	ways = 0
	# branch out all ways with this denom 
	while( num_of_denom * denom <= n ):
		ways += count_coin_change_ways(n - num_of_denom * denom, next_denom)
		num_of_denom += 1

	return ways


print(count_coin_change_ways(100, 25))