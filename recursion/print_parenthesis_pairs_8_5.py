
# print all valid pairs of parenthesis given n


def print_all_parenthesis( n, left, right, string):

	if( right == n ):
		print(string)
		return

	if right < left:
		print_all_parenthesis(n, left, right + 1, string + ")")

	if( left < n):
		print_all_parenthesis(n, left + 1, right, string + "(")
	
	

print_all_parenthesis(4, 0, 0, "")
