# hogyun choi august 31 2021

'''
8.7

Q: given infite number of quarters, dimes, nickles, and pennies, write code to calculate the number of ways to represent n cents


n = 100

all_ways_0_quarters + all_ways_1_quarters + all_ways_2_quarters + etc.... 

all_ways_0_quarters_0_dimes + all_ways_0_quarter_1_dimes + all_ways_0_quarters_2_dimes + all_ways_0_quarter_3_dimes

'''

def count_all_ways_of_change( n , denom ):
	
	next_denom = 0
	if denom == 25:
		next_denom = 10
	elif denom == 10:
		next_denom = 5
	elif denom == 5:
		next_denom = 1
	else:
		return 1  # remainder in pennies, found a way!

	ways = 0

	i = 0
	while( i * denom <= n ):
		ways += count_all_ways_of_change(n - i *denom, next_denom)
		i += 1

	return ways 



print(count_all_ways_of_change(100, 25))
