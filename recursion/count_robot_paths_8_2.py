
# hogyun choi / august 31, 2021
# robot on NxN map and can only go right or down, how many paths can the robot take?

# warning: apparently this solution is not efficient!

map = [ [0, 0, 0, 0, 0, 0, 0],
		   [0, 1, 0, 0, 0, 0, 0],
		   [0, 0, 0, 0, 0, 0, 0],
		   [0, 0, 0, 1, 0, 0, 0],
		   [0, 0, 0, 0, 0, 0, 0],
		   [0, 0, 1, 0, 0, 0, 0],
		   [0, 0, 0, 0, 0, 0, 0] ]

map = [	[0,0,0],
		[0,0,0],
		[0,0,0]]

seen_map_positions = {}

def count_robot_paths( map , y, x  ):
	
	if( y >= len(map) or len(map) == 0): return 0
	if( x >= len(map[0])): return 0
	if( x == len(map[0]) - 1 and y == len(map) - 1): return 1
	if( map[y][x] != 1):
		return count_robot_paths( map, y + 1, x) + count_robot_paths( map, y, x + 1)
	else:
		return 0

print(count_robot_paths(map, 0, 0))


	
