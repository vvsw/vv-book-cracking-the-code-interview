
# hogyun choi august 31 2021
# paint fill question
# given a screen (m x n) of colors, and a point, and a color, 
# fill in surrounding until you hit a border of that color


# the x and y are all switched !!!! warning, need to fix

def fill_in_paint( screen, point_x, point_y, color):
	
	if len(screen) == 0 or len(screen[0]) == 0:
		return

	if point_x < 0 or point_x >= len(screen):
		return

	if point_y < 0 or point_y >= len(screen[0]):
		return

	if screen[point_x][point_y] == color:
		return
	
	screen[point_x][point_y] = color  # fill this pixel with color

	# try to fill in surrounding pixels
	fill_in_paint(screen, point_x, point_y - 1 , color)
	fill_in_paint(screen, point_x, point_y + 1 , color)
	fill_in_paint(screen, point_x - 1, point_y , color)
	fill_in_paint(screen, point_x + 1, point_y , color)


def print_screen_colors( screen ):
	for row in screen:
		print(row)


screen = [ [1, 1, 1, 0, 0, 1, 1],
		   [1, 1, 0, 0, 0, 1, 0],
		   [1, 1, 1, 1, 1, 1, 1],
		   [1, 1, 1, 0, 0, 0, 1],
		   [1, 1, 1, 0, 0, 1, 1] ]

fill_in_paint( screen, 1, 2, 1)
print_screen_colors(screen )

