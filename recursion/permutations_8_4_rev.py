# hogyun choi august 31, 2021

# print all permutations of a string

def permutations(prefix, string):
	if(len(string) == 0):
		print(prefix)
		return

	# give every char in string chance to be the next char in prefix
	for char in string:
		prefix_with_char = prefix + char
		string_without_char = [x for x in string if x != char]
		permutations( prefix_with_char, string_without_char )



permutations("", "12345")