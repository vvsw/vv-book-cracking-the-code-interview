
# note: screen[row][col]  == screen[y][x]  when origin is top left
# hogyun choi august 31 2021

def paint_fill_color( screen , point_x, point_y, color):
	
	if len(screen) == 0:
		return
	if point_y < 0 or point_y >= len(screen):
		return
	if point_x < 0 or point_x >= len(screen[0]):
		return

	if screen[point_y][point_x] == color:
		return

	screen[point_y][point_x] = color

	paint_fill_color( screen, point_x - 1, point_y, color) # left
	paint_fill_color( screen, point_x + 1, point_y, color) # right
	paint_fill_color( screen, point_x, point_y - 1, color) # up
	paint_fill_color( screen, point_x, point_y + 1, color) # down

def print_screen_colors( screen ):
	for row in screen:
		print(row)


screen = [ [1, 1, 1, 0, 0, 1, 1],
		   [1, 0, 0, 0, 0, 1, 0],
		   [1, 0, 1, 1, 1, 1, 1],
		   [1, 0, 1, 0, 0, 0, 1],
		   [1, 0, 1, 0, 0, 1, 1] ]

paint_fill_color( screen , 3, 0, 1)
print_screen_colors(screen)


'''
$ python fill_screen_paint_8_6_rev.py 
[1, 1, 1, 1, 1, 1, 1]
[1, 1, 1, 1, 1, 1, 0]
[1, 1, 1, 1, 1, 1, 1]
[1, 1, 1, 0, 0, 0, 1]
[1, 1, 1, 0, 0, 1, 1]
'''