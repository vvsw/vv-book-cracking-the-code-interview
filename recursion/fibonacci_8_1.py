

# def generate the nth fibonaci

memo = {}

def fibonacci(n, memo):
	if(n == 0):
		return 0
	if(n == 1):
		memo[n] = 1
	if( n > 1 ):
		if n not in memo:
			memo[n] = fibonacci(n - 2, memo) + fibonacci(n - 1, memo)
	return memo[n]


print(fibonacci(10,{}))