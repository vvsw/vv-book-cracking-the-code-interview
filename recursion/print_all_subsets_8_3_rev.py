
# hogyun choi/ august 31, 2021 
# print all subsets given a string 

# note: its like permutations, but once char is used, dont include it again

def print_all_subsets( subset, string ):
	
	for char in string:
		subset_with_char = subset + char
		print(subset_with_char)

		string_without_char = [ x for x in string if x != char ]
		print_all_subsets(subset_with_char, string_without_char )

		# we found all subsets with this char
		# dont include it again in l:10
		string = string_without_char


print_all_subsets( "", "JKLMOP" )