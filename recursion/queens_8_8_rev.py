# hogyun choi august 31 2021
# all boards of placing 8 queens

col_for_row = [0, 0, 0, 0, 0, 0 ,0 ,0]

def place_the_queen( row ):
	# all rows have a queen
	if( row == 8 ):
		print_the_board()
		return

	# try placing queen in each col
	for col in xrange(0, 8):
		col_for_row[ row ] = col

		if( check( row )):
			# if valid, keep placement and try next row
			# only valid queen placements will get next row
			place_the_queen( row + 1)


# check if queen on most recent row is valid
def check( row ):
	# for all previous queens up to this row, 
	# check if conditions hold

	for current_row_number in xrange(0, row):
		prev_queen_col = col_for_row[current_row_number]
		
		# find col distance between the two queens
		col_distance_val = abs(col_for_row[row] - col_for_row[current_row_number])

		# find the row distance between two queens
		row_distance_val = row - current_row_number

		if( col_distance_val == 0 or row_distance_val == col_distance_val ):
			return False
	return True

def print_the_board():
	tmp_board_arr = [0,0,0,0,0,0,0,0]
	for row in xrange(0, len(col_for_row)):
		col = col_for_row[row]
		tmp_board_arr[col] = 1
		print(tmp_board_arr)
		
		tmp_board_arr[col] = 0
	print("\n")


place_the_queen(0)
	