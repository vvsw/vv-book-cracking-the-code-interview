# hogyun choi august 31 2021

#notes: 
# look into xrange for going backwards


# 8.8  print all ways of arranging eight queens so
# that none of them share row col diagonal

# diagonal check = if distance of height == distance of width
# diagnoal check = if col1 - col2 == row1 - row2

# each index is a row, value is which column is occupied
# represents the whole board, all 8 rows



column_in_row = [0,0,0,0,0,0,0,0]		

# check if queen can be placed in row, relative to previous queens
def check( row ):
	for i in xrange(0, row):
		col_distance = abs(column_in_row[i] - column_in_row[row])
		row_distance = row - i

		if( col_distance == 0 or row_distance == col_distance):
			return False
	return True

def place_the_queen( row ):

	if( row == 8 ):
		print_the_board()
		return

	for col in xrange(0, 8):
		# try placing queen in every col of this row
		column_in_row[row] = col

		if( check(row) ):
			place_the_queen(row + 1)

def print_the_board():
	tmp_board_arr = [0,0,0,0,0,0,0,0]
	for row in xrange(0, len(column_in_row)):
		col = column_in_row[row]
		tmp_board_arr[col] = 1
		print(tmp_board_arr)
		
		tmp_board_arr[col] = 0
	print("\n")




place_the_queen(0)