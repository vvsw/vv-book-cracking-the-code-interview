
# hogyun choi / september 1, 2021 
# Q: 1.8 is string s2 a rotation of s1



def is_string_substring(sub ,string):
	return sub in string

def is_string_rotated(s1, s2):
	if( len(s1) != len(s2)):
		return False
	return is_string_substring(s2, s1 + s1)


print(is_string_rotated("waterbottle", "erbottlewat" )) 
print(is_string_rotated("hello","lohel"))
print(is_string_rotated("12345","4512"))
print(is_string_rotated("12345","45124"))