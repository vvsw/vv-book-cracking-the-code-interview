'''
hogyun choi / september 1, 2021

Q1.3: remove duplicates from a string

iterate through each char, keep index of next available spot
if char not seen before, place in next available spot and then increment index

when reached the end, zero out the string from where the index is to end of string

'''


def remove_duplicates_in_string( string ):
	

	seen_chars_dict = {}
	next_available_index = 1
	seen_chars_dict[ next_available_index ] = True

	# go through each char and if not seen, place in index and increment index
	for i in xrange(1, len(string)):

		if string[i] not in seen_chars_dict:
			seen_chars_dict[string[i]] = True
			string[next_available_index] = string[i]
			next_available_index += 1

		else:
			pass

	# clear out the rest of duplicates
	for x in xrange( next_available_index, len(string)):
		string[x] = 0

	return string


print( remove_duplicates_in_string( [1,4,5,1,2,3,1,1,1,4,5,1,1,4,4,5,7])) 
