



'''

hogyun choi / september 1, 2021 
Q1.2: reverse string


'''



def reverse_string_in_place( string ):

	start = 0
	end = len(string) - 1

	while( start < end ):
		tmp  = string[start]
		string[start] = string[end]
		string[end] = tmp
		start += 1
		end -= 1
	return string


print(reverse_string_in_place( ['h','e','l','l','o','1','2','3'] ))