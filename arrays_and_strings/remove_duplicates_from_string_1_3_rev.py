# hogyun choi / september 1, 2021 

# remove duplicates from string

# start at char(i=1), and keep track of a next_available_index=1
# check all chars upto next_available_index for char(i)
# if a duplicate exists, increment i and look for next char(i+1)
# if not, place char(i) into next_available_index and increment next_available_index
# and now look for next char(i+1)

# $ python remove_duplicates_from_string_1_3_rev.py 
# [1, 4, 5, 3, 7, 0, 0, 0, 0, 0, 0, 0]
# [2, 4, 5, 1, 3, 9, 7, 0, 0, 0, 0, 0]


def remove_duplicates_in_string( string ):
	
	next_available_index = 1
	for i in xrange(1, len(string)):
		j = 0
		while( j < next_available_index ):
			# check if char(i) exists between 0 and next_available_index

			# if we find a char(i), then go to next char(i+1)
			if( string[j] == string[i]):
				break
			j += 1
		# we didnt find a char(i), so place it in index
		# and increment index
		# now check for next char(i+1)
		if( j == next_available_index):
			string[next_available_index] = string[i]
			next_available_index += 1


	# all duplicates found, zero out the rest
	for x in xrange(next_available_index, len(string)):
		string[x] = 0

	return string


print( remove_duplicates_in_string([1,4,5,1,4,5,3,3,3,3,7,1]))



def remove_duplicates_in_string2( string ):
	
	seen_chars_dict = {}
	next_available_index = 1

	seen_chars_dict[string[0]] = True
	for i in xrange(1, len(string)):
		if string[i] not in seen_chars_dict:
			seen_chars_dict[string[i]] = True
			string[next_available_index] = string[i]
			next_available_index += 1
		else:
			pass #


	# all duplicates found, zero out the rest
	for x in xrange(next_available_index, len(string)):
		string[x] = 0

	return string


print( remove_duplicates_in_string2([2,4,5,1,4,5,3,3,9,3,7,1]))

