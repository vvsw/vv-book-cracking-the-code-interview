
'''
hogyun choi / september 1, 2021 
Q: 1.8

is s2 a rotation of s1, ie: waterbottle , bottlewater

'''

def is_string_rotated( s1, s2 ):
	if len(s2) != len(s1):
		return False
	return s2 in (s1 + s1)

print( is_string_rotated("waterbottle", "bottlewater"))
print( is_string_rotated("12345", "45123"))
print( is_string_rotated("12345", "45512"))
print( is_string_rotated("12345", "234512"))

