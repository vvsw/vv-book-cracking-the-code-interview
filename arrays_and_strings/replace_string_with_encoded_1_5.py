'''
hogyun choi / september 1, 2021
Q1.5 

given string, replace all spaces with '%20'

$ python replace_string_with_encoded_1_5.py 
hello%20my%20name%20is%20john!
['h', 'e', 'l', 'l', 'o', '%', '2', '0', 't', 'h', 'e', 'r', 'e']
['l', 'o', 'w', '%', '2', '0', 'i', 's', '%', '2', '0', 'h', 'i', 'g', 'h', '%', '2', '0', 'a', 'n', 'd', '%', '2', '0', 't', 'h', 'e', 'r', 'e', '%', '2', '0', 'i', 's', '%', '2', '0', 'n', 'o', 't', 'h', 'i', 'n', 'g', '%', '2', '0', 't', 'o', '%', '2', '0', 's', 'h', 'o', 'w', '!']


'''

def replace_space_with_encoded( string ):
	
	n = len(string)
	num_of_spaces = 0

	for char in string:
		if char == " ":
			num_of_spaces += 1

	new_encoded_string = ""

	for char in string:
		if char != " ":
			new_encoded_string += char
		else:
			new_encoded_string += "%20"

	return new_encoded_string


print(replace_space_with_encoded("hello my name is john!"))


def replace_space_with_encoded_2( string ):
	n = len(string)

	num_of_spaces = 0
	for char in string:
		if char == " ":
			num_of_spaces += 1

	new_size_string = [0] * (n + 2*num_of_spaces)

	i = 0 # original string index
	j = 0 # new string index
	while( j < len(new_size_string)):
		if string[i] != " ":
			new_size_string[j] = string[i]
			i += 1
			j += 1
		else:
			new_size_string[j] = '%'
			new_size_string[j+1] = '2'
			new_size_string[j+2] = '0'
			j += 3
			i += 1
	return new_size_string

print(replace_space_with_encoded_2("hello there"))
print(replace_space_with_encoded_2("low is high and there is nothing to show!"))
