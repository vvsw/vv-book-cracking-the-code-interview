
'''
hogyun choi / september 1, 2021
Q: 1.7
given MxN matrix, if element is 0, make the whole column and row of that element 0


matrix = [ 	[1,1,1,1,1],
			[1,0,1,1,1],
			[1,1,1,1,1],
			[1,1,1,0,1]  ]

$ python column_row_zero_1_7.py 
[1, 0, 1, 0, 1]
[0, 0, 0, 0, 0]
[1, 0, 1, 0, 1]
[0, 0, 0, 0, 0]


'''


def column_row_zero( matrix ):

	if len(matrix) == 0:
		return

	rows_with_zero = [0] * len(matrix)
	columns_with_zero = [0] * len(matrix[0])

	for row in xrange(0, len(matrix)):
		for col in xrange(0, len(matrix[row])):
			if matrix[row][col] == 0:
				rows_with_zero[row] = 1
				columns_with_zero[col] = 1

	# go through and zero out rows and columns
	for row in xrange(0, len(matrix)):
		for col in xrange(0, len(matrix[row])):
			if rows_with_zero[row] == 1:
				matrix[row][col] = 0

			if columns_with_zero[col] == 1:
				matrix[row][col] = 0

	return matrix



def print_matrix( m ):
	for row in m:
		print(row)


matrix = [ 	
		[1,1,1,1,1],
		[1,0,1,1,1],
		[1,1,1,1,1],
		[1,1,1,0,1],
		[1,1,1,1,1]
		]

m = column_row_zero( matrix )
print_matrix( m )