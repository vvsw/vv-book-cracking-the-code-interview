'''

given a matrix NxN, rotate by 90 degrees


matrix = [ 	[1, 2, 3, 4, 5],
			[6, 7, 8, 9, 10],
			[11,12,13,14,15],
			[16,17,18,19,20],
			[21,22,23,24,25]  ]
'''

'''
topl -> topr : 		m[0][n] = m[0][0]
topr -> bottomr : 	m[n][n] = m[0][n]
bottomr -> bottml:  m[n][0] = m[n][n]
bottoml -> topl:	m[0][0] = m[n][0]

topl -> topr : 		m[0+1][n] = m[0][0+1]
topr -> bottomr : 	m[n][n-1] = m[0+1][n]
bottomr -> bottml:  m[n-1][0] = m[n][n-1]
bottoml -> topl:	m[0][0+1] = m[n-1][0]



...

topl -> topr : 		m[0+l+1][n-l] = m[0+l][0+1+l]
topr -> bottomr : 	m[n-l][n-1-l] = m[0+1+l][n-l]
bottomr -> bottml:  m[n-1-l][0+l] = m[n-l][n-1-l]
bottoml -> topl:	m[0+l][0+1+l] = m[n-1-l][0+l]

...

topl -> topr : 		m[l+i][n-l] = m[l][i+l]
topr -> bottomr : 	m[n-l][n-i-l] = m[i+l][n-l]
bottomr -> bottml:  m[n-i-l][l] = m[n-l][n-i-l]
bottoml -> topl:	m[l][i+l] = m[n-i-l][l]

for( int l=0; l < n/2; l++)
	for( int i = 0; i < n - l; i++ )
		tmp = m[l+i][n-l]
		m[l+i][n-l] = m[l][i+l]
		m[l][i+l] = m[n-i-l][l]
		m[n-i-l][l] = m[n-l][n-i-l]
		m[n-l][n-i-l] = tmp
	
'''	

# matrix = [ 	[1, 2, 3, 4, 5],
# 			[6, 7, 8, 9, 10],
# 			[11,12,13,14,15],
# 			[16,17,18,19,20],
# 			[21,22,23,24,25]  ]

# [21, 16, 11, 6, 1]
# [22, 17, 12, 7, 2]
# [23, 18, 13, 8, 3]
# [24, 19, 14, 9, 4]
# [25, 20, 15, 10, 5]



matrix = [ 	[1,  2,  3,  4,   5, 6],
			[7,  8,  9,  10, 11, 12],
			[13, 14, 15, 16, 17, 18],
			[19, 20, 21, 22, 23, 24],
			[25, 26, 27, 28, 29, 30],
			[31, 32, 33, 34, 35, 36],  ]

# [31, 25, 19, 13, 7, 1]
# [32, 26, 20, 14, 8, 2]
# [33, 27, 15, 16, 9, 3]
# [34, 28, 21, 22, 10, 4]
# [35, 29, 23, 17, 11, 5]
# [36, 30, 24, 18, 12, 6]




def rotate_matrix_90( m ):

	if len(m) == 0 or len(m) != len(m[0]):
		assert(False)
		return m
	n = len(m) - 1
	for l in xrange(0, n/2):
		for i in xrange(0, n-l-l):
			tmp = m[l+i][n-l]
			print("layer={}, offset={}").format(l, i)

			print("m[{}][{}] {} -> m[{}][{}] {}").format(l, i+l, m[l][i+l], l+i, n-l, m[l+i][n-l]) 
			m[l+i][n-l] = m[l][i+l]
			print("m[{}][{}] {} -> m[{}][{}] {}").format(n-i-l, l, m[n-i-l][l], l, i+l, m[l][i+l])
			m[l][i+l] = m[n-i-l][l]
			print("m[{}][{}] {} -> m[{}][{}] {}").format(n-l, n-i-l, m[n-l][n-i-l], n-i-l, l, m[n-i-l][l])
			m[n-i-l][l] = m[n-l][n-i-l]
			print("m[{}][{}] {} -> m[{}][{}] {}").format(l+i, n-l, tmp, n-l, n-i-l, m[n-l][n-i-l])
			m[n-l][n-i-l] = tmp
			print("\n")
	return m
	

def print_matrix( m ):
	for row in m:
		print(row)

m = rotate_matrix_90(matrix)

print_matrix(m)
