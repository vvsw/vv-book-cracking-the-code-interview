'''

hogyun choi / september 1, 2021
Q: 1.7 given a matrix, make whole row 0 if there is 0 in that row,
make whole column 0 if that column has a 0


'''


def row_columns_zero( matrix ):
	
	if( len(matrix) == 0 ):
		return matrix

	rows_with_zero = [0] * len(matrix)
	columns_with_zero = [0] * len(matrix[0])


	# iterate and mark rows/cols with zeros
	for row in xrange( 0, len(matrix)):
		for col in xrange( 0, len(matrix[row])):

			if matrix[row][col] == 0:
				rows_with_zero[row] = 1
				columns_with_zero[col] = 1

	# zero out the rows and columns previously marked
	for row in xrange( 0, len(matrix)):
		for col in xrange( 0, len(matrix[row])):

			if rows_with_zero[row] == 1:
				matrix[row][col] = 0
			if columns_with_zero[col] == 1:
				matrix[row][col] = 0

	return matrix

def print_matrix( m ):
	for row in m:
		print(row)

matrix = [ 	[1,1,1,1,1],
			[1,0,1,1,1],
			[1,1,1,1,1],
			[1,1,1,0,1],
			[1,1,1,1,1]  ]

print_matrix( row_columns_zero( matrix ) )