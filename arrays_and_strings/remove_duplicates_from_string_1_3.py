
# hogyun choi / september 1, 2021
# Q1.3 remove duplicates from string without additonal buffer


# abbaccde

def remove_duplicates_in_string( string ):
	next_available_index = 0
	for i in xrange(1, len(string)):
		j = 0
		while( j < next_available_index ):
			# check up to index if char(i) exists
			if( string[j] == string[i] ):
				# char at i already exists at j
				# move to next char(i+1) by incrementing i
				break
			j += 1
		if( j == next_available_index ):
			# char at i not seen up to new index, so place it there
			string[next_available_index] = string[i]
			next_available_index += 1

	for x in xrange(next_available_index, len(string)):
		string[x] = 0
	return string
print( remove_duplicates_in_string([1,2,1,1,3,1,4,2,2,3,3,1,5,2]))
print( remove_duplicates_in_string([4,4,5,4,4,1,2,6,6,6,6,1,9]))


