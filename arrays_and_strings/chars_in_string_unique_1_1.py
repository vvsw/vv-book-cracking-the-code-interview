'''
hogyun choi / september 1, 2021

q:1.1

check if string consists of all unique chars


'''

# check all chars before i, to see if it existed before

def chars_in_string_unique( string ):
	for i in xrange(0, len(string)):
		for j in xrange(0, i):
			if string[i] == string[j]:
				return False
	return True


print( chars_in_string_unique("abcdeaf") )
assert True == ( chars_in_string_unique("abcdefg") )

def chars_in_string_unique_memo( string ):
	seen_chars_vector = [0] * 256
	for char in string:
		i = ord(char)
		if seen_chars_vector[i]:
			return False
		else:
			seen_chars_vector[i] = True
	return True

def chars_in_string_unique_sort( string ):
	previous_char_seen = ''
	for char in sorted(string):
		if previous_char_seen == char:
			return False
		previous_char_seen = char
	return True


print(chars_in_string_unique_memo("abcdeaf"))
print(chars_in_string_unique_memo("abcdefg"))
print(chars_in_string_unique_sort("absdfsgessa"))
print(chars_in_string_unique_sort("absdfwer"))