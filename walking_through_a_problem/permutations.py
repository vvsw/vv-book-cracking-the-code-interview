
# page 62/708 on edition 6 of ctci by gayle laakman

# june 10, 2021 hgc e. 

# implementation of permutations() -> void
# 	: prints all the permutations of a given list

# not the ACTUAL BOOK SOLUTION


# STRING ARE IMMUTABLE

# def permutations( prefix, string ):
# 	if len(string) == 0:
# 		print( str( prefix ) )
# 		return
# 	else:
# 		# each letter gets to be put into prefix individually
# 		# new prefix and new string is passed into permutations()

# 		for i in range(0, len(string)):
# 			prefix = str(prefix) + string[i]
# 			string = [ x for j,x in enumerate(string) if j != i] # everything in string but el at i
# 			print("prefix={}  string={}").format(prefix, string)
# 			permutations( prefix, dstring)


# def print_all_permutations( string ):
# 	assert( type(string) == str )
# 	permutations("", string)


# print_all_permutations("ABCD")

def permutations( prefix, arr ):

	if len(arr) == 0:
		print( str( prefix ) )
	else:
		for i in range(0, len(arr)):
			el = arr[i]
			prefix_with_el = prefix + [el]
			arr_without_el = [ x for j,x in enumerate(arr) if j != i]
			permutations( prefix_with_el, arr_without_el )










# example test cases
permutations([], ['f','a','g','g', 'o', 't', 'r', 'y'])
permutations([], ['1','2','3','4', '5', '6', '7', '8'])
permutations([], ['1','2','3'])